package com.example.springbootdemo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @RequestMapping("/hi")
    public String sayHi(){
        return "hello Spring boot";
    }

    @RequestMapping("/bye")
    public String sayBye(){
        return "Bye!";
    }

}